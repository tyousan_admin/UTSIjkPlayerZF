Pod::Spec.new do |spec|

  spec.name         = "UTSIjkPlayerZF"
  spec.version      = "1.0.1"
  spec.summary      = "UTSIjkPlayerZF."
  spec.description  = <<-DESC
  IJKPlayer for DCloud
                   DESC
  
  spec.homepage     = "https://gitee.com/tyousan_admin/UTSIjkPlayerZF"
  spec.license      = { :type => 'MIT', :file => 'LICENSE' }
  spec.author             = { "176142998" => "176142998@qq.com" }
  spec.source       = { :git => "https://gitee.com/tyousan_admin/UTSIjkPlayerZF.git", :tag => spec.version}
  
  spec.vendored_frameworks = 'IJKMediaFramework.framework'
  spec.frameworks  = "AudioToolbox", "AVFoundation", "CoreGraphics", "CoreMedia", "CoreVideo", "MobileCoreServices", "OpenGLES", "QuartzCore", "VideoToolbox", "Foundation", "UIKit", "MediaPlayer"
  spec.libraries   = "bz2", "z", "stdc++"

  spec.platform = :ios
  spec.ios.deployment_target = '12.0'
  spec.requires_arc = true

  spec.prepare_command = <<-CMD
    tar -xvf ./IJKMediaFramework.txz
  CMD
  
  spec.user_target_xcconfig = {
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64',
  }
end
